# Todo #

* Create social media presence and post to it: Facebook, Twitter, Tumblr, Pinterest, Instagram, LinkedIn, Xing. Link back and forth from and to website.
* Create favicon (incl. all mobile icons).
* Minify images.
* google search terms how we would like to find christa: these keywords +synonyms need to be in the content.
* Opengraph metatags. Make it easy to share christa on social platforms.

# Done #

* Mobile media queries.
* Fix menu on mobile.
* Fix the cache header for images (It's 4h right now).
* Define hover effects for interactive elements.
* Prepare responsive images.
* Inline Ruxit agent (possible with next production release).
* **Google Webmaster Tools!!!**
* Deploy first set of images to instagram, to make the page not look so empty.
* Add robots.txt.
* Build sitemap.xml on deploy.
* Fetch images from Instagram and add them to the static build.
* Everything else.
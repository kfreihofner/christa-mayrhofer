require "mini_magick"
require "yaml"
require "uglifier"
require_relative "overrides"
require_relative "instagram"

Jekyll::Hooks.register :site, :after_reset do |site|
  beg = Time.now

  puts "register start..."

  # Fetch instagram metadata and resize the images
  # to a number of images with different resolutions for
  # responsive handling.
  instagram = Instagram.new("christa.mayrhofer")
  instagram.posts.each do |post|
    puts post.local_path

    for i in 1..9
      width = 120 * i
      post.create_thumbnail_at("/img/werke/#{width}/#{post.id}.jpg", width)
    end
    post.create_thumbnail_at("/img/werke/1/#{post.id}.jpg", 1)
  end

  puts "Instagram thumbnails created..."

  # Store the instagram metadata to a data file for later
  # access within the templates.
  metadata_path = File.join(SRC_DIR, "_data", "instagram.yml")
  FileUtils.mkdir_p File.dirname(metadata_path)
  File.open(metadata_path, 'w') { |file| YAML.dump(instagram, file) }

	puts "Finished post_read in #{'%.1f' % (Time.now-beg)}s."
end

Jekyll::Hooks.register :site, :post_read do |site|
  beg = Time.now

  # Prepare inline javascript
  js = ""
  Dir.glob("#{SRC_DIR}/_js/[!_]*").each do |input|
    js << File.open(input, "rb").read
  end
  js_uglified = Uglifier.compile(js, :comments => :none)
  site.data['js'] = js_uglified

  # Prepare Ruxit javascript inline tag
  ruxit = Net::HTTP.get(URI.parse("https://rtk85726.live.ruxit.com/api/v1/rum/jsInlineScript/APPLICATION-EA134053A2EBE434?Api-Token=G-EwPU-lQAyG79fnOXngw"))
  site.data['ruxit'] = ruxit

  puts "Finished post_read in #{'%.1f' % (Time.now-beg)}s."
end

Jekyll::Hooks.register :site, :pre_render do |site|
  beg = Time.now
  puts "Finished pre_render in #{'%.1f' % (Time.now-beg)}s."
end

Jekyll::Hooks.register :site, :post_render do |site|
  beg = Time.now
  puts "Finished post_render in #{'%.1f' % (Time.now-beg)}s."
end

Jekyll::Hooks.register :site, :post_write do |site|
  beg = Time.now
  puts "Finished post_write in #{'%.1f' % (Time.now-beg)}s."
end

require "mini_magick"

SRC_DIR = Jekyll.configuration({})['source']
DST_DIR = Jekyll.configuration({})['destination']

def puts(o)
  now = Time.now
  super("#{now.strftime("%H:%M:%S")} #{o}")
  now
end

def format_duration(seconds)
  minutes = (seconds / 60.0).to_i
  hours = (minutes / 60.0).to_i

  formatted = ""
  if hours >= 1
    formatted = "#{hours}h #{(minutes - 60 * hours).to_i}min"
  elsif minutes >= 1
    formatted = "#{minutes}min #{(seconds - 60 * minutes).to_i}s"
  else
    formatted = "#{'%.1f' % seconds}s"
  end
  return formatted
end

module MiniMagick
  class Image
    def color_at x, y
      run_command("convert", "#{path}[1x1+#{x.to_i}+#{y.to_i}]", 'txt:').split("\n").each do |line|
        return $1 if /^0,0:.*(#[0-9a-fA-F]+)/.match(line)
      end
      nil
    end
  end
end

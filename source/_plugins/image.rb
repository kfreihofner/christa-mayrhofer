class Image
  attr_reader :local_path

  def initialize(local_path)
    @local_path = local_path
  end

  def to_liquid
    {
      "local_path" => @local_path
    }
  end
end

require "net/http"
require "uri"
require "json"
require_relative "overrides"

class Thumbnail
  attr_reader :width
  attr_reader :local_path
  attr_reader :uri

  def initialize(width, uri, source)
    @width = width
    @uri = uri
    @local_path = File.join(SRC_DIR, uri)

    unless File.exist?@local_path
      FileUtils.mkdir_p File.dirname(@local_path)

      original_image = MiniMagick::Image.open(source)
      original_image.combine_options do |c|
        c.filter "Lanczos"
        c.unsharp "0x0.5"
        c.thumbnail "#{width}x#{width}^"
        c.gravity "center"
        c.extent "#{width}x#{width}"
        c.strip
        c.profile.+ "*"
        c.interlace "none"
        c.quality "84"
      end
      original_image.write @local_path
    end
  end

  def to_liquid
    {
      "width" => @width,
      "local_path" => @local_path,
      "uri" => @uri
    }
  end
end

class Post
  attr_reader :id
  attr_reader :code
  attr_reader :uri
  attr_reader :caption
  attr_reader :thumbnail_src
  attr_reader :display_src
  attr_reader :local_path
  attr_reader :thumbnails
  attr_reader :primary_color

  def initialize(id, code, caption, thumbnail_src, display_src, local_path)
    @id = id
    @code = code
    @uri = "https://www.instagram.com/p/#{@code}/"
    @caption = caption
    @thumbnail_src = thumbnail_src
    @display_src = display_src
    @local_path = local_path
    @thumbnails = Hash.new
    @primary_color = nil

    # Download image if necessary
    unless File.exist?local_path
      FileUtils.mkdir_p(File.dirname(local_path))

      uri = URI.parse(@display_src)
      Net::HTTP.start(uri.host) do |http|
        response = http.get(uri.request_uri)
        open(local_path, "wb") do |file|
          file.write(response.body)
        end
      end
    end
  end

  def create_thumbnail_at(uri, width)
    puts "Creating #{width}px thumbnail for #{@id} at #{uri}..."

    thumbnail = Thumbnail.new(width, uri, @local_path)
    if width == 1
      puts "DEBUG DEBUG DEBUG!!! Fetching pixie color from #{thumbnail.local_path}..."
      #pixie = MiniMagick::Image.open(thumbnail.local_path)
      #@primary_color = pixie.color_at(0, 0)
    end
    @thumbnails[width] = thumbnail
  end

  def to_liquid
    {
      "id" => @id,
      "code" => @code,
      "uri" => @uri,
      "caption" => @caption,
      "thumbnail_src" => @thumbnail_src,
      "display_src" => @display_src,
      "local_path" => @local_path,
      "thumbnails" => @thumbnails,
      "primary_color" => @primary_color
    }
  end
end

class Instagram
  attr_reader :username
  attr_reader :profile_pic_src
  attr_reader :posts

  def initialize(username)
    @username = username

    # Grab HTML from instagram.com
    uri = URI.parse("https://www.instagram.com/#{username}/")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    body = response.body

    # Parse JSON metadata from HTML
    start_index = body.index('window._sharedData = ') + 'window._sharedData = '.length
    end_index = body.index('};</script>', start_index)
    json = body[start_index..end_index]
    metadata = JSON.parse(json)

    # Extract metadata
    @profile_pic_src = metadata["entry_data"]["ProfilePage"][0]["user"]["profile_pic_url"]
    @posts = Array.new
    nodes = metadata["entry_data"]["ProfilePage"][0]["user"]["media"]["nodes"]
    nodes.each do |node|
      id = node["id"]
      code = node["code"]
      caption = node["caption"]
      thumbnail_src = node["thumbnail_src"]
      display_src = node["display_src"]
      local_path = "#{SRC_DIR}/_img-cache/instagram/#{@username}/#{id}.jpg"
      @posts << Post.new(id, code, caption, thumbnail_src, display_src, local_path)
    end
  end

  def to_liquid
    {
      "username" => @username,
      "profile_pic_src" => @profile_pic_src,
      "posts" => @posts
    }
  end
end

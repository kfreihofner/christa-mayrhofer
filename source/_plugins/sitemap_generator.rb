module Jekyll

  class Sitemap < Page
    def initialize(site, base)
      @site = site
      @base = base
      @name = 'sitemap.xml'

      self.process(@name)
      self.read_yaml(File.join(base, '_layouts'), 'sitemap.xml')

      self.data['lastmod'] = Time.new.utc.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    end
  end

  class SitemapGenerator < Generator
    def generate(site)
      if site.layouts.key? 'sitemap'
        site.pages << Sitemap.new(site, site.source)
      end
    end
  end

end
